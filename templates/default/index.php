<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.default
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */

$app  = JFactory::getApplication();
$user = JFactory::getUser();

// Output as HTML5
$this->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if ($task === 'edit' || $layout === 'form')
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add template js
// JHtml::_('script', 'template.js', array('version' => 'auto', 'relative' => true));

// Add html5 shiv
JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));

// Add Stylesheets
JHtml::_('stylesheet', 'template.min.css', array('version' => 'auto', 'relative' => true));


// Template color

// Check for a custom CSS file
JHtml::_('stylesheet', 'user.css', array('version' => 'auto', 'relative' => true));

// Check for a custom js file
JHtml::_('script', 'user.js', array('version' => 'auto', 'relative' => true));

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);


// Logo file or site title param

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="apple-touch-icon" sizes="57x57" href="templates/<?php echo $this->template ?>/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="templates/<?php echo $this->template ?>/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="templates/<?php echo $this->template ?>/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="templates/<?php echo $this->template ?>/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="templates/<?php echo $this->template ?>/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="templates/<?php echo $this->template ?>/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="templates/<?php echo $this->template ?>/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="templates/<?php echo $this->template ?>/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="templates/<?php echo $this->template ?>/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="templates/<?php echo $this->template ?>/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="templates/<?php echo $this->template ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="templates/<?php echo $this->template ?>/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="templates/<?php echo $this->template ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="templates/<?php echo $this->template ?>/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="templates/<?php echo $this->template ?>/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy|Roboto:400,500" rel="stylesheet">

	<?php
		unset($this->_scripts[JURI::root(true).'/media/jui/js/bootstrap.min.js']);
		unset($this->_scripts[JURI::root(true).'/media/jui/js/jquery.min.js']);
	?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="templates/<?php echo $this->template ?>/js/template.js"></script>

	<jdoc:include type="head" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129643584-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-129643584-1');
	</script>

</head>
<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '')
	. ($this->direction === 'rtl' ? ' rtl' : '');
?>">

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = 'https://connect.facebook.net/it_IT/sdk/xfbml.customerchat.js';
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
attribution=setup_tool
page_id="1895608597343966"
theme_color="#f73d4a"
logged_in_greeting="Ciao! Come possiamo aiutarti?"
logged_out_greeting="Ciao! Come possiamo aiutarti?">
</div>

	<jdoc:include type="modules" name="H1" style="xhtml" />

	<!-- Body -->
	<div class="body" id="top">



		<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white shadow">
			<a class="navbar-brand lh-sm d-flex align-items-center" href="<?php echo $this->baseurl; ?>">
				<div class="logo mr-10">
					<img src="templates/<?php echo $this->template ?>/images/logo.png" width="70" class="d-none d-md-block"/>
					<img src="templates/<?php echo $this->template ?>/images/logo.png" width="50" class="d-block d-md-none"/>
				</div>
				<h3 class=" h4 text-primary lh-sm mb-0">Il Mare di Roma
					<small class="d-block text-secondary">Ostia lido</small>
				</h3>
			</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<jdoc:include type="modules" name="MainMenu" style="none" />
			</div>
		</nav>


		<!-- Header -->
		<header class="header pb-4" role="banner" style="padding-top: 100px">
			<?php if ($this->countModules('HeaderContainer')) : ?>
			<div class="container d-none d-md-block">
				<div class="swiper-container swiper-container-header shadow my-4">
					<jdoc:include type="modules" name="HeaderContainer" style="none" />
				</div>
			</div>
			<?php endif; ?>
			<?php if ($this->countModules('HeaderFull')) : ?>
			<div class="bg-primary py-15 d-none d-md-block">
				<div class="container">
						<div class="swiper-container swiper-container-header shadow my-4">
							<jdoc:include type="modules" name="HeaderFull" style="none" />
						</div>
				</div>
			</div>
			<?php endif; ?>
		</header>


		<main id="content" role="main" class="">
			<div class="container">

				<jdoc:include type="modules" name="position-2" style="none" />

				<!-- Begin Content -->
				<jdoc:include type="message" />
				<div class="row align-items-center">

					<div class="<?php if ($this->countModules('ArticleRight')) : echo 'col-lg-4'; else: echo 'col-sm-12'; endif; ?> ">
						<jdoc:include type="component" />
					</div>

					<?php if ($this->countModules('ArticleRight')) : ?>
						<div class="col-lg-8">
							<jdoc:include type="modules" name="ArticleRight" style="none" />
						</div>
					<?php endif; ?>

				</div>
				<!-- End Content -->
			</div>

			<jdoc:include type="modules" name="BoxBottom" style="xhtml" />

			<?php if ($this->countModules('WhyChoose')) : ?>
			<div class="container my-40 py-40">
				<h3 class="h3 mb-20">Perché sceglierci</h3>
				<div class="row">
					<jdoc:include type="modules" name="WhyChoose" style="xhtml" />
				</div>
			</div>
			<?php endif ?>

			<?php if ($this->countModules('WhyChoose')) : ?>
				<jdoc:include type="modules" name="OffersList" style="xhtml" />
			<?php endif ?>

		</main>

			<jdoc:include type="modules" name="banner" style="xhtml" />

	</div>
	<!-- Footer -->
	<footer class="footer my-60 pt-40" role="contentinfo">
		<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">

			<jdoc:include type="modules" name="footer" style="xhtml" />

			<div class="my-40 py-30">
				<h4 class="text-center mb-15">Scopri dove sono le nostre strutture</h4>
				<div class="row">
					<jdoc:include type="modules" name="footerAddress" style="xhtml" />
				</div>
			</div>

			<!-- <p class="pull-right">
				<a href="#top" id="back-top">
					<?php# echo JText::_('TPL_DEFAULT_BACKTOTOP'); ?>
				</a>
			</p> -->
		</div>
		<div class="container text-center">
			<p class="text-sm">Jessica Silvestri P.I. 13169051003 - &copy; <?php echo date('Y'); ?> <?php echo $sitename; ?></p>
		</div>
	</footer>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
