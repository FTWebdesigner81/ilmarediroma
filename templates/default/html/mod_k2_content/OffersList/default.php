<?php
/**
 * @version    2.8.x
 * @package    K2
 * @author     JoomlaWorks http://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2017 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="container ">
	<?php if(count($items)): ?>
  <div class="d-flex flex-wrap ">

    <?php foreach ($items as $key=>$item):	?>
			<?php foreach ($item->extra_fields as $extraField): ?>
				<?php if($extraField->value != ''):
					switch ($extraField->alias) {
						case 'discount':
							$discount = $extraField->value;
							break;
					}
				endif;
			endforeach; ?>

		<div class="col-md-6 col-lg-4">
			<a href="<?php echo $item->link; ?>" class="card card-link card-tipology shadow border-0 mb-15">
			<?php if($params->get('itemImage')) : ?>
				<img class="card-img-top" src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>">
			<?php endif ?>

			<div class="card-body">
				<?php if($params->get('itemTitle')): ?>
					<h4 class="h4 card-title mb-0 lh-1">
						<?php echo $item->title; ?>
					</h4>
				<?php endif; ?>
				<div class="h5 mb-0"><strong><?php echo $discount ?></strong></div>
			</div>

			<div class="card-footer bg-white d-flex align-items-center">
				<div class="ml-auto">
					<div class="btn btn-secondary">Dettaglio</div>
				</div>
			</div>
	    </a>
		</div>
    <?php endforeach; ?>

  </div>
  <?php endif; ?>

	<?php if($params->get('itemCustomLink')): ?>
	<a class="moduleCustomLink" href="<?php echo $itemCustomLinkURL; ?>" title="<?php echo K2HelperUtilities::cleanHtml($itemCustomLinkTitle); ?>"><?php echo $itemCustomLinkTitle; ?></a>
	<?php endif; ?>


</div>
