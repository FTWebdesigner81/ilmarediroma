<?php
/**
 * @version    2.8.x
 * @package    K2
 * @author     JoomlaWorks http://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2017 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="swiper-container swiper-container-review">
<?php if(count($items)): ?>

		<div class="swiper-wrapper">

  	<?php foreach ($items as $key=>$item):	?>

			<?php foreach ($item->extra_fields as $extraField): ?>
				<?php if($extraField->value != ''):
					switch ($extraField->alias) {
						case 'author':
							$author = $extraField->value;
							break;
						case 'date':
							$date = $extraField->value;
							break;
						case 'vote':
							$vote = $extraField->value;
							break;
						case 'source':
							$source = $extraField->value;
							break;
					}
				endif;
			endforeach; ?>

	    <div class="swiper-slide flex-column text-left">
				<div class="d-flex align-items-center mb-10 w-100">
					<i class="ico ico-review text-32 text-blue-soft mr-5"></i>
					<div class="mr-5">
						<div class="badge badge-primary rounded py-5">
							<?php echo $vote ?>
						</div>
					</div>
					<div class="lh-1">
						<?php echo $author ?><br>
						<small><?php echo $date ?></small>
					</div>
				</div>
				<blockquote>
					<?php if($params->get('itemIntroText')): ?>
						<div class="text-md">
							<?php echo $item->introtext; ?>
						</div>
					<?php endif; ?>
					<p class="text-md"><?php echo $source ?></p>
				</blockquote>
    	</div>
    <?php endforeach; ?>
		</div>

		<div class="swiper-button position-absolute" style="top: 0; right: 0; z-index: 10">
			<div class="swiper-button-prev btn btn-circle btn-primary">
				<i class="ico ico-arrow-left"></i>
			</div>
			<div class="swiper-button-next btn btn-circle btn-primary">
				<i class="ico ico-arrow-right"></i>
			</div>
		</div>
  <?php endif; ?>
  </div>
