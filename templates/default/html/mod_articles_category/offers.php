<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<div class="container ">
	<div class="card-deck">
		<?php foreach ($list as $item) : ?>
			<div class="col-sm-4">
				<a class="card card-link card-tipology shadow border-0" href="<?php echo $item->link; ?>" >
					<?php $images = json_decode($item->images); ?>
					<img class="card-img-top" src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>">
					<div class="card-body">
						<h3 class="card-title"><?php echo $item->title; ?></h3>
			      <p class="card-text"><?php echo $item->fulltext; ?></p>
					</div>
					<div class="card-footer bg-white d-flex align-items-center">
						<div class="h3">-15%</div>
						<div class="ml-auto">
							<div class="btn btn-secondary">Dettaglio</div>
						</div>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
</div>
