<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<div class="row <?php echo $moduleclass_sfx; ?>">
	<?php foreach ($list as $item) : ?>
		<div class="col-sm-6">
			<a class="card card-link card-tipology shadow" href="<?php echo $item->link; ?>" >
				<img class="card-img-top height-lg" src="http://via.placeholder.com/350x300" alt="Card image cap">
				<div class="card-body">
					<div class="d-flex">
						<h3 class="card-title">
							<?php echo $item->displayIntrotext; ?>
						</h3>
						<div class="ml-auto">
							<span class="btn btn-circle btn-primary">
								<i class="ico ico-arrow-right"></i>
							</span>
						</div>
					</div>
				</div>
			</a>
		</div>
	<?php endforeach; ?>
</div>
