<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="card-deck">
	<div class="card bg-dark text-white border-0 shadow">
		<img class="card-img" src="<?php echo ($params->get('backgroundimage_01')) ?>" alt="Card image">
	</div>
	<div class="card bg-dark text-white border-0 shadow">
		<img class="card-img " src="<?php echo ($params->get('backgroundimage_02')) ?>" alt="Card image">
	</div>
</div>
