<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>


<!-- Additional required wrapper -->

	<div class="swiper-wrapper">

		<!-- Slides -->
		<?php if ($params->get('backgroundimage_01')) : ?>
		<div class="swiper-slide">
			<div class="position-absolute pos-left pos-bottom p-15">
				<h3 class="mb-0 text-white" style="text-shadow: 1px 1px 8px rgba(0,0,0,.5)"><?php echo ($params->get('captionimage_01')) ?></h3>
			</div>
			<img src="<?php echo ($params->get('backgroundimage_01')) ?>">
		</div>
		<?php endif; ?>

		<?php if ($params->get('backgroundimage_02')) : ?>
		<div class="swiper-slide">
			<div class="position-absolute pos-left pos-bottom p-15">
				<h3 class="mb-0 text-white" style="text-shadow: 1px 1px 8px rgba(0,0,0,.5)"><?php echo ($params->get('captionimage_02')) ?></h3>
			</div>
			<img src="<?php echo ($params->get('backgroundimage_02')) ?>">
		</div>
		<?php endif; ?>

		<?php if ($params->get('backgroundimage_03')) : ?>
		<div class="swiper-slide">
			<div class="position-absolute pos-left pos-bottom p-15">
				<h3 class="mb-0 text-white" style="text-shadow: 1px 1px 8px rgba(0,0,0,.5)"><?php echo ($params->get('captionimage_03')) ?></h3>
			</div>
			<img src="<?php echo ($params->get('backgroundimage_03')) ?>">
		</div>
		<?php endif; ?>

	</div>

	<!-- Add Pagination -->
	<div class="swiper-pagination swiper-pagination-white"></div>

	<div class="swiper-button position-absolute pos-right pos-bottom p-15" style="z-index: 10">
		<div class="swiper-button-prev btn btn-circle btn-primary">
			<i class="ico ico-arrow-left"></i>
		</div>
		<div class="swiper-button-next btn btn-circle btn-primary">
			<i class="ico ico-arrow-right"></i>
		</div>
	</div>

<!-- If we need pagination -->
