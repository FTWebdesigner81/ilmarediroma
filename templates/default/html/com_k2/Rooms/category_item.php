<?php
/**
 * @version    2.8.x
 * @package    K2
 * @author     JoomlaWorks http://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2017 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

?>

<?php foreach ($this->item->extra_fields as $key=>$extraField):
	if($extraField->value != '') :
		switch ($extraField->alias) {
			case 'service':
				$arrayService = $extraField->value;
				break;
			case 'address':
				$address = $extraField->value;
				break;
			// Review
			case 'reviewer':
				$reviewer = $extraField->value;
				break;
			case 'review':
				$review = $extraField->value;
				break;
			case 'title':
				$title = $extraField->value;
				break;
			case 'avatar':
				$avatar = $extraField->value;
				break;
			case 'score':
				$score = $extraField->value;
				break;
			case 'source':
				$source = $extraField->value;
				break;
			case 'cta_description':
				$ctaDescription = $extraField->value;
				break;
			case 'cta_button':
				$ctaButton = $extraField->value;
				break;
			case 'IDBooking':
				$IDBooking = $extraField->value;
				break;
		}
	endif;
endforeach; ?>

<?php
	$arrayServiceExplode = explode(", ", $arrayService);
	$arrayServiceCount = count($arrayServiceExplode);
?>

<!-- Start K2 Item Layout -->
<div class="mb-30 py-30 <?php echo ucfirst($this->item->itemGroup); ?><?php echo ($this->item->featured) ? ' catItemIsFeatured' : ''; ?><?php if($this->item->params->get('pageclass_sfx')) echo ' '.$this->item->params->get('pageclass_sfx'); ?>">

	<div class="page-header d-md-flex align-items-end mb-10">
		<?php if($this->item->params->get('catItemTitle')): ?>
		<!-- Item title -->
		<h3 class="catItemTitle mb-0 lh-1">
			<?php echo $this->item->title; ?>
			<?php if($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
			<!-- Featured flag -->
			<span>
				<sup>
					<?php echo JText::_('K2_FEATURED'); ?>
				</sup>
			</span>
			<?php endif; ?>
		</h3>

		<div class="ml-auto text-md text-bold text-uppercase">
			<i class="ico ico-pin"></i> <?php echo $address ?>
		</div>
		<?php endif; ?>
	</div>

	<div class="container">
	<div class="row">
		<div class="col-md-3 card card-body border-0 ">

		<!-- Plugins: BeforeDisplay -->
		<?php echo $this->item->event->BeforeDisplay; ?>

		<!-- K2 Plugins: K2BeforeDisplay -->
		<?php echo $this->item->event->K2BeforeDisplay; ?>


	  <!-- Plugins: AfterDisplayTitle -->
	  <?php echo $this->item->event->AfterDisplayTitle; ?>

	  <!-- K2 Plugins: K2AfterDisplayTitle -->
	  <?php echo $this->item->event->K2AfterDisplayTitle; ?>


	  <!-- Plugins: BeforeDisplayContent -->
	  <?php echo $this->item->event->BeforeDisplayContent; ?>

	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  <?php echo $this->item->event->K2BeforeDisplayContent; ?>

		  <?php if($this->item->params->get('catItemIntroText')): ?>
		  <!-- Item introtext -->
		  <div class="list-decoration text-md">
		  	<?php echo $this->item->introtext; ?>
		  </div>
		  <?php endif; ?>

			<!-- Function for icon stamp -->
			<?php
				$count = 0;
				while ($count < $arrayServiceCount) {
					echo '<span class="badge badge-secondary mr-1">' . $arrayServiceExplode[$count] . '</span>';
					$count++;
				}
			?>

		  <!-- Plugins: AfterDisplayContent -->
		  <?php echo $this->item->event->AfterDisplayContent; ?>

		  <!-- K2 Plugins: K2AfterDisplayContent -->
		  <?php echo $this->item->event->K2AfterDisplayContent; ?>


		  <!-- Plugins: AfterDisplay -->
		  <?php echo $this->item->event->AfterDisplay; ?>

		  <!-- K2 Plugins: K2AfterDisplay -->
		  <?php echo $this->item->event->K2AfterDisplay; ?>
		</div>

		<?php if($this->item->params->get('catItemImageGallery') && !empty($this->item->gallery)): ?>
	  <!-- Item image gallery -->
	  <div class="col-md-9">
		  <?php echo $this->item->gallery; ?>
	  </div>
	  <?php endif; ?>
		</div>
		</div>
		<div class="container">
			<div class="row card-deck my-15">
				<?php if ($ctaDescription): ?>
				<div class="col-md-3 card card-body bg-primary text-white text-center d-flex align-items-center justify-content-center">
					<p><?php echo $ctaDescription ?></p>
					<a href="<?php echo $IDBooking ?  $IDBooking : $ctaButton ?>" class="btn btn-info">Verifica Disponibilità</a>
				</div>
				<?php endif ?>


				<?php if ($reviewer || $review ) : ?>

					<div class="col-md card card-body border border-secondary">
						<div class="d-flex align-items-center">
							<i class="ico ico-review text-32 text-blue-soft mr-5"></i>
							<h4 class="h4 mb-0 mt-5"><?php echo $title ?></h4>

							<?php if ($score) : ?>
							<h3 class="h4 mb-0 lh-1 mr-5 ml-auto">
								<div class="rounded py-5">
									<span class="mt-5 d-block">Voto: <?php echo $score ?></span>
								</div>
							</h3>
							<?php endif ?>
						</div>
						<blockquote class="font-italic ">
							<?php echo $review ?>
							<footer class="blockquote-footer text-primary">Recensito da <?php echo $reviewer ?> su <span class="text-bold"><?php echo $source ?></span></footer>
						</blockquote>
					</div>

				<?php endif; ?>
			</div>
		</div>


</div>
<!-- End K2 Item Layout -->
