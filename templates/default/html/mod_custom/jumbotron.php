<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<div class="jumbotron jumbotron-fluid bg-primary text-white <?php echo $moduleclass_sfx; ?> my-4 position-relative">
	<div class="position-absolute pos-top pos-bottom d-flex" style="z-index: 0; right: 10%">
		<i class="ico ico-fish op-3" style="font-size: 420px"></i>
	</div>

	<div class="container d-md-flex justify-content-between align-items-center py-4 position-relative">
		<?php echo $module->content; ?>
	</div>
</div>
