<?php
/**
 * @version     3.1.x
 * @package     Simple Image Gallery Pro
 * @author      JoomlaWorks - http://www.joomlaworks.net
 * @copyright   Copyright (c) 2006 - 2016 JoomlaWorks Ltd. All rights reserved.
 * @license     http://www.joomlaworks.net/license
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<div id="sigProId<?php echo $gal_id; ?>"
	class="sigProContainer swiper-container swiper-container-gallery sigProCustom<?php echo $singleThumbClass.$extraWrapperClass; ?> ">

	<div class="swiper-wrapper align-items-end pt-15">

	<?php foreach($gallery as $count=>$photo): ?>
		<a href="<?php echo $photo->sourceImageFilePath; ?>"
			<?php if($gal_singlethumbmode && $count>0) echo ' style="display:none !important;"'; ?>
			class="show-on-hover-toggle animation-gallery sigProLink<?php echo $extraClass; ?> sigProThumb card height-lg shadow-sm swiper-slide border-0"
			rel="<?php echo $relName; ?>[gallery<?php echo $gal_id; ?>]"
			title="<?php echo $photo->captionDescription.$photo->downloadLink.$modulePosition; ?>"
			data-fresco-caption="<?php echo $photo->captionDescription.$photo->downloadLink.$modulePosition; ?>"
			target="_blank"<?php echo $customLinkAttributes; ?>>
			<div class="show-on-hover show-on-bottom w-100 text-center position-absolute transition-all" style="z-index:2">
				<i class="ico ico-photo text-white text-48"></i>
			</div>
			<?php if(($gal_singlethumbmode && $count==0) || !$gal_singlethumbmode): ?>
			<img class="card-img sigProImg bg-cover "
				src="<?php echo $transparent; ?>"
				style="background-image:url('<?php echo $photo->thumbImageFilePath; ?>'); background-repeat: no-repeat;"
				alt="<?php echo JText::_('JW_SIGP_LABELS_08').' '.$photo->filename; ?>"
				title="<?php echo JText::_('JW_SIGP_LABELS_08').' '.$photo->filename; ?>"
			/>
			<?php endif; ?>
		</a>
	<?php endforeach; ?>

	</div>

	<div class="swiper-pagination"></div>

	<div class="swiper-button position-absolute" style="top: 0px; right: 0; z-index: 10">
		<div class="swiper-button-prev btn btn-circle btn-primary">
			<i class="ico ico-arrow-left"></i>
		</div>
		<div class="swiper-button-next btn btn-circle btn-primary">
			<i class="ico ico-arrow-right"></i>
		</div>
	</div>
</div>

<?php if(isset($flickrSetUrl)): ?>
	<a class="sigProFlickrSetLink" title="<?php echo $flickrSetTitle; ?>" target="_blank" href="<?php echo $flickrSetUrl; ?>"><?php echo JText::_('JW_SIGP_PLG_FLICKRSET'); ?></a>
<?php endif; ?>

<?php if($itemPrintURL): ?>
<div class="sigProPrintMessage">
	<?php echo JText::_('JW_SIGP_PLG_PRINT_MESSAGE'); ?>:
	<br />
	<a title="<?php echo $row->title; ?>" href="<?php echo $itemPrintURL; ?>"><?php echo $itemPrintURL; ?></a>
</div>
<?php endif; ?>
