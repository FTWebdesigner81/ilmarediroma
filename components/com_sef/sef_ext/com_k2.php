<?php
/**
 * K2 SEF extension for ARTIO JoomSEF
 * 
 * @package   JoomSEF
 * @author    ARTIO s.r.o., http://www.artio.net
 * @copyright Copyright (C) 2015 ARTIO s.r.o. 
 * @license   GNU/GPLv3 http://www.artio.net/license/gnu-general-public-license
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access.');

class SefExt_com_k2 extends SefExt {
    var $params;
    var $paginationLimit = 14;
    var $oldSuffix = null;

    function getNonSefVars(&$uri) {
        $this->_createNonSefVars($uri);

        return array($this->nonSefVars, $this->ignoreVars);
    }

    function _createNonSefVars(&$uri) {
        if (isset($this->nonSefVars) && isset($this->ignoreVars))
            return;

        $this->nonSefVars = array();
        $this->ignoreVars = array();

        if (!is_null($uri->getVar('limit')))
            $this->nonSefVars['limit'] = $uri->getVar('limit');
        if ($this->params->get('sef_pagination', '0') == '0') {
            if (!is_null($uri->getVar('limitstart')))
                $this->nonSefVars['limitstart'] = $uri->getVar('limitstart');
        }
    }

    function AddNamePart(&$name, $object, $part) {
        if (isset($object->$part)) {
            $name[] = $object->$part;
        }
    }

    function BuildName($object, $fieldname, $defaultText) {
        $name = array();
        $object->text = $this->params->get($fieldname.'text', $defaultText);
        $this->AddNamePart($name, $object, $this->params->get($fieldname.'1', 'none'));
        $this->AddNamePart($name, $object, $this->params->get($fieldname.'2', 'title'));
        $this->AddNamePart($name, $object, $this->params->get($fieldname.'3', 'none'));

        return implode('-', $name);
    }

    function getCategoryTitle($id, $forceLast = false) {
        $id = intval($id);
        
        $cats = $this->params->get('category_inc', '2');
        $categories = array();

        if ($cats == '0' && !$forceLast)
            $id = 0;
        while ($id > 0) {
            $database = JFactory::getDBO();
            $database->setQuery("SELECT id, name AS title, alias, parent FROM #__k2_categories WHERE id = ".$id);
            $row = $database->loadObject();
            if (is_null($row)) {
                return null;
            }

            $name = $this->BuildName($row, 'catname', 'Category');
            array_unshift($categories, $name);

            $id = $row->parent;
            if ($cats != '2')
                break;
        }
        return $categories;
    }

    function getItemTitle($id) {
        $id = intval($id);
        
        $database = JFactory::getDBO();
        $database->setQuery("SELECT id, title, alias, catid FROM #__k2_items WHERE id =".$id);
        $row = $database->loadObject();
        if (is_null($row)) {
            return null;
        }

        $name = $this->BuildName($row, 'itemname', 'Item');

        if ($this->params->get('cat_item_inc', '1') == '1') {
            $category = $this->getCategoryTitle($row->catid);
            if (is_null($category)) {
                return null;
            }

            array_push($category, $name);
            return $category;
        }
        else {
            return array($name);
        }
    }

    function getAuthorTitle($id) {
        $id = intval($id);
        
        $column = ($this->params->get('authorname', 'name'));
        
        $database = JFactory::getDBO();
        $database->setQuery('SELECT id, '.$column.' FROM #__users WHERE id = '.$id);
        $rows = $database->loadRow();
        if (is_null($rows)) {
            return null;
        }

        $name = (($this->params->get('authorid_inc', '0') != '0') ? $id.'-' : '').$rows[1];

        return $name;
    }

    function getFileInfo($id) {
        $id = intval($id);
        
        $db = JFactory::getDbo();
        $db->setQuery("SELECT * FROM #__k2_attachments WHERE id = ".intval($id));
        $row = $db->loadObject();

        return $row;
    }

    function getCategories($id, $recursive) {
        $id = intval($id);
        
        $categories = array();
        $db = JFactory::getDBO();
        $sefConfig = SEFConfig::getConfig();

        // JF translate extension.
        $jfTranslate = $sefConfig->translateNames ? ', id' : '';

        while ($id > 0) {
            $db->setQuery("SELECT name, parent{$jfTranslate} FROM #__k2_categories WHERE id = ".$id);
            $row = $db->loadObject();
            if (is_null($row)) {
                return null;
            }

            array_unshift($categories, $row->name);

            $id = $row->parent;
            if (!$recursive)
                break;
        }

        return $categories;
    }

    function loadMetaData($type, $id) {
        $database = JFactory::getDBO();
        $sefConfig = SEFConfig::getConfig();

        // JF translate extension.
        $jfTranslate = $sefConfig->translateNames ? ', `id`' : '';

        $descField = $metakeySource = null;

        switch ($type) {
            case 'item':
                $sql = "SELECT `title`, `catid`, `introtext`, CONCAT(`introtext`, ' ', `fulltext`) AS `text`$jfTranslate FROM `#__k2_items` WHERE `id` = '".(int)$id."'";
                $descField = 'introtext';
                $metakeySource = 'text';
                break;

            case 'category':
                $sql = "SELECT `name` AS `title`, `description`, `parent` AS `catid`$jfTranslate FROM `#__k2_categories` WHERE `id` = '".(int)$id."'";
                $descField = 'description';
                $metakeySource = 'description';
                break;

            case 'user':
                $sql = "SELECT `name` AS `title` FROM `#__users` WHERE `id` = '".(int)$id."'";
                $descField = 'title';
                $metakeySource = 'title';
                break;

            default:
                $sql = '';
        }

        if (!empty($sql)) {
            $database->setQuery($sql);
            $row = $database->loadObject();

            if (isset($row->$descField))
                $this->metadesc = $row->$descField;
            if (isset($row->$metakeySource))
                $this->metakeySource = $row->$metakeySource;
            if (isset($row->metakey))
                $this->origmetakey = $row->metakey;
            if (isset($row->metadesc))
                $this->origmetadesc = $row->metadesc;

            if (isset($row->title)) {
                $titleParts = array($row->title);

                // Add categories?
                $addCatsToCat = $this->params->get('meta_catcat', '0');
                $addCatsToItem = $this->params->get('meta_itemcat', '0');

                if (($type == 'item' && $addCatsToItem != '0') || ($type == 'category' && $addCatsToCat != '0')) {
                    // Recursive?
                    $recursive = ($type == 'item' && $addCatsToItem == '2');
                    $cats = $this->getCategories($row->catid, $recursive);
                    if ($cats != null) {
                        if ($type == 'item' && $this->params->get('meta_itemcat_type', '0') == '0') {
                            // Append
                            $titleParts = array_merge($titleParts, $cats);
                        }
                        else {
                            // Prepend
                            $titleParts = array_merge($cats, $titleParts);
                        }
                    }
                }

                // Separator
                $sep = trim($this->params->get('meta_cat_sep', ''));
                if ($sep == '') {
                    $sep = ' ';
                }
                else {
                    $sep = ' '.$sep.' ';
                }

                // Build title
                $this->metatitle = implode($sep, $titleParts);
            }

            return true;

        }
        elseif ($type == 'tag') {
            $this->metadesc = $id;
            $this->metakeySource = $id;
            $this->metatitle = 'Tag: '.$id;

            return true;
        }

        return false;

    }

    function googleNews($title, $id) {
        $num = '';
        $add = $this->params->get('google_news', '0');

        if ($add == '1') {
            // Article ID
            $digits = trim($this->params->get('google_news_digits', '2'));
            if (!is_numeric($digits)) {
                $digits = '3';
            }

            $num = sprintf('%0'.$digits.'d', $id);
        }
        else
            if ($add == '2') {
                // Publish date
                $db = JFactory::getDBO();
                $query = "SELECT `publish_up` FROM `#__k2_items` WHERE `id` = '$id'";
                $db->setQuery($query);
                $time = $db->loadResult();

                $time = strtotime($time);

                $date = $this->params->get('google_news_dateformat', 'ddmm');

            $search = array('dd', 'd', 'mm', 'm', 'yyyy', 'yy');
            $replace = array(date('d', $time), date('j', $time), date('m', $time), date('n', $time), date('Y', $time), date('y', $time));
                $num = str_replace($search, $replace, $date);
            }

        if (!empty($num)) {
            $where = $this->params->get('google_news_pos', '1');

            $sep = $this->params->get('google_news_sep', '');
            if (empty($sep)) {
                $sefConfig = SEFConfig::getConfig();
                $sep = $sefConfig->replacement;
            }

            if ($where == '1') {
                $title = $title.$sep.$num;
            }
            else {
                $title = $num.$sep.$title;
            }
        }

        return $title;
    }

    function addGoogleNews(&$title, $id) {
        $id = intval($id);
        
        // Get and remove last part of title
        $name = array_pop($title);

        // Generate Google News number from name
        $name = $this->googleNews($name, $id);

        // Handle the slash correctly
        $name = explode('/', $name);

        // Add back to URL parts
        $title = array_merge($title, $name);
    }

    function afterCreate(&$uri)
    {
        // Restore old suffix in configuration if set
        if (!is_null($this->oldSuffix)) {
            $sefConfig = SEFConfig::getConfig();
            $sefConfig->suffix = $this->oldSuffix;
        }
    }
    
    function beforeCreate(&$uri) {
        // Remove the part after ':' from variables
        if (!is_null($uri->getVar('id'))) {
            // Don't fix the id if it contains token
            if (!preg_match('/^[0-9]+_[a-z0-9]+$/i', $uri->getVar('id'))) {
                SEFTools::fixVariable($uri, 'id');
            }
        }

        // 12.11.2012 dajo: Remove layout only if there's already the same task set
        $layout = $uri->getVar('layout');
        if (!is_null($layout)) {
            if ($uri->getVar('task') == $layout) {
                $uri->delVar('layout');
            }
            // 27.11.2012 dajo: Remove layout for view=item&layout=item (generates duplicate URLs)
            elseif ($uri->getVar('view') == 'item' && $layout == 'item') {
                $uri->delVar('layout');
            }
        }

        // Remove empty task
        if ($uri->getVar('task') == '') {
            $uri->delVar('task');
        }

        // Remove empty id
        if ($uri->getVar('id') == '') {
            $uri->delVar('id');
        }
        
        // Remove empty limitstart
        $limitstart = $uri->getVar('limitstart');
        if ($limitstart == '' || $limitstart == '0') {
            $uri->delVar('limitstart');
        }
    }
    
    function getPageLimit($params) {
        $limit = $params->get('num_leading_items', 2);
        $limit += $params->get('num_primary_items', 4);
        $limit += $params->get('num_secondary_items', 4);
        $limit += $params->get('num_links', 4);
        
        $this->paginationLimit = $limit;
    }
    
    function getCategoryPageLimit($id) {
        $id = intval($id);
        $db = JFactory::getDbo();
        $db->setQuery("SELECT `params` FROM `#__k2_categories` WHERE `id` = '{$id}'");
        $params = $db->loadResult();
        
        if (is_string($params)) {
            $params = new JRegistry($params);
            $this->getPageLimit($params);
        }
    }
    
    function getMenuPageLimit($id) {
        $id = intval($id);
        $menuItem = JoomSEF::_getMenuItemInfo(null, null, $id);
        if ($menuItem && isset($menuItem->params) && is_object($menuItem->params)) {
            $this->getPageLimit($menuItem->params);
        }
    }
    
    function getMenuCategory($id) {
        $id = intval($id);
        $menuItem = JoomSEF::_getMenuItemInfo(null, null, $id);
        if ($menuItem && isset($menuItem->params) && is_object($menuItem->params)) {
            $cat = $menuItem->params->get('categories');
            if (is_array($cat))
                $cat = reset($cat);
            if (is_numeric($cat))
                return intval($cat);
        }
        
        return null;
    }

    function create(&$uri) {
        $vars = $uri->getQuery(true);
        extract($vars);
        $title = array();

        $this->params = SEFTools::getExtParams('com_k2');

        // If Itemid not set, don't add menu title if not set to
        if ($this->params->get('menu_always', '1') == '1' || isset($Itemid)) {
            $title[] = JoomSEF::_getMenuTitle(isset($option) ? $option : null, null, isset($Itemid) ? $Itemid : null);
        }

        if (isset($view)) {
            switch ($view) {
                case 'itemlist':
                    unset($view);
                    break;
                case 'item':
                    // Handle download links
                    if (isset($task) && ($task == 'download')) {
                        if (empty($id)) {
                            return $uri;
                        }

                        // Parse download file ID
                        $parts = explode('_', $id);
                        $did = $parts[0];

                        // Get download file info
                        $info = $this->getFileInfo($did);
                        if (is_null($info)) {
                            return null;
                        }

                        $fileTitle = $info->title;
                        $id = $info->itemID;
                        unset($task);
                    }
                    if (isset($id)) {
                        $itemTitle = $this->getItemTitle($id);
                        if (is_null($itemTitle)) {
                            return $uri;
                        }
                        $title = array_merge($title, $itemTitle);
                        $this->loadMetaData('item', $id);
                    }
                    if ($this->params->get('google_news', '0') != '0') {
                        $this->addGoogleNews($title, $id);
                    }
                    if (!empty($fileTitle)) {
                        $title[] = 'Download';
                        $title[] = $fileTitle;
                    }
                    unset($view);
                    break;
                default:
                    $title[] = $view;
            }
        }

        // Whether to add index.php to URL
        $addIndex = false;
        
        if (!isset($task) && isset($layout)) {
            $task = $layout;
        }
        if (isset($task)) {
            switch ($task) {
                case 'category':
                    if (isset($id)) {
                        $this->getCategoryPageLimit($id);
                    }
                    elseif (isset($Itemid)) {
                        $this->getMenuPageLimit($Itemid);
                        
                        if ($this->params->get('cat_from_menu', 0))
                            $id = $this->getMenuCategory($Itemid);
                    }
                    
                    if (isset($id) && !empty($id)) {
                        $cats = $this->getCategoryTitle($id, true);
                        if (is_null($cats)) {
                            return $uri;
                        }
                        $title = array_merge($title, $cats);
                        $this->loadMetaData('category', $id);
                    }
                    
                    // Add index.php if set to
                    if ($this->params->get('cat_legacy_index', 0))
                        $addIndex = count($title);
                    
                    unset($task);
                    break;
                case 'tag':
                	if ($this->params->get('tags_add_menu_item', '0') == '0') {
                    	$title = array();
                	}
                    if ($this->params->get('tags_inc', '1') == '1') {
                        $title[] = JText::_('TAGS');
                    }
                    if (isset($tag)) {
                    	if ($this->params->get('tags_inc_tag', '1') == '1') {
                        	$title[] = $tag;
                    	}
                        $this->loadMetaData('tag', $tag);
                    }
                    $k2params = JComponentHelper::getParams('com_k2');
                    $this->paginationLimit = $k2params->get('tagItemCount', 10);
                    unset($task);
                    break;
                case 'user':
                    if ($this->params->get('author_add_menu_item', '1') == '0') {
                        $title = array();
                    }
                    if ($this->params->get('author_inc', '1') == '1') {
                        $title[] = JText::_('Author');
                    }
                    if (isset($id)) {
                        $tmpTitle = $this->getAuthorTitle($id);
                        if (is_null($tmpTitle)) {
                            return $uri;
                        }
                        $title[] = $tmpTitle;
                        $this->loadMetaData('user', $id);
                    }
                    $k2params = JComponentHelper::getParams('com_k2');
                    $this->paginationLimit = $k2params->get('userItemCount', 10);
                    unset($task);
                    break;
                case 'edit':
                    if (isset($cid)) {
                        $itemTitle = $this->getItemTitle($cid);
                        if (is_null($itemTitle)) {
                            return $uri;
                        }
                        $title = array_merge($title, $itemTitle);
                    }
                    $title[] = JText::_('Edit');
                    unset($task);
                    break;
                case 'search':
                    $title[] = JText::_('Search');
                    if (!empty($searchword))
                        $title[] = $searchword;
                    $k2params = JComponentHelper::getParams('com_k2');
                    $this->paginationLimit = $k2params->get('genericItemCount', 10);
                    unset($task);
                    unset($searchword);
                    break;
                case 'date':
                    $title[] = 'date';
                    if (!empty($year))
                        $title[] = $year;
                    if (!empty($month))
                        $title[] = $month;
                    if (!empty($day))
                        $title[] = $day;
                    $k2params = JComponentHelper::getParams('com_k2');
                    $this->paginationLimit = $k2params->get('genericItemCount', 10);
                    unset($task);
                    unset($year);
                    unset($month);
                    unset($day);
                    break;
                case 'latest':
                    // Don't add
                    unset($task);
                    break;
                default:
                    $title[] = $task;
            }
        }
        
        // Handle pagination
        if (isset($limitstart) && ($this->params->get('sef_pagination', '0') == '1')) {
            if (intval($this->paginationLimit) == 0) {
                $this->paginationLimit = 1;
            }
            $pageNum = intval(intval($limitstart) / $this->paginationLimit) + 1;
            if ($pageNum > 1) {
                $pagetext = strval($pageNum);
                $sefConfig = SEFConfig::getConfig();
                if (($cnfPageText = $sefConfig->getPageText())) {
                    $pagetext = str_replace('%s', $pageNum, $cnfPageText);
                    $title[] = $pagetext;
                }
            }
        }

        if (isset($format))
            $title[] = $format;

        if (isset($type))
            $title[] = $type;

        if (isset($print) && $print == '1')
            $title[] = JText::_('PRINT');
        
        // Add index.php if set to and no URL part was added to $title
        if ($addIndex && count($title) == $addIndex) {
            $title[] = 'index';
            $sefConfig = SEFConfig::getConfig();
            $this->oldSuffix = $sefConfig->suffix;
            $sefConfig->suffix = '.php';
        }

        // Generate meta tags
        $metatags = $this->getMetaTags();
        if (isset($this->metatitle)) {
            $metatags['metatitle'] = $this->metatitle;
        }

        $this->_createNonSefVars($uri);

        $newUri = $uri;
        if (count($title) > 0)
            $newUri = JoomSEF::_sefGetLocation($uri, $title, null, null, null, @$lang, $this->nonSefVars, null, $metatags, null, true);
        return $newUri;
    }
}
?>