## Grunt plugin

### grunt-contrib-sass
Compile Sass to CSS

* npm install grunt-contrib-sass --save-dev]


### grunt-contrib-cssmin
Minify CSS

* npm install grunt-contrib-cssmin --save-dev]


### grunt-contrib-concat
Concatenate files.

* npm install grunt-contrib-concat --save-dev]


### grunt-contrib-uglify
Minify JavaScript files with UglifyJS

* npm install grunt-contrib-uglify --save-dev]


### grunt-path
Returns information about each file in the specified directory

* npm install grunt-path --save-dev


### grunt-contrib-jshint
Validate files with JSHint

* npm install grunt-contrib-jshint --save-dev


### grunt-webfont
Generate webfont from SVG

* brew install ttfautohint fontforge --with-python
* npm install grunt-webfont --save-dev


### grunt-contrib-watch
Run predefined tasks whenever watched file patterns are added, changed or deleted

* npm install grunt-contrib-watch --save-dev
