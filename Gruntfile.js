module.exports = function(grunt) {
  grunt.initConfig({
    sass: {                              // Task
      dist: {                            // Target
        options: {                       // Target options
          style: 'expanded'
        },
        files: {                         // Dictionary of files
          'templates/default/css/template.css': ['assets/stylesheets/*.sass', 'assets/stylesheets/*.scss'],
          'templates/styleguides/css/template.css': ['assets/stylesheets/*.sass', 'assets/stylesheets/*.scss'],
        }
      }
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'templates/default/css/template.min.css' : 'templates/default/css/template.css',
          'templates/styleguides/css/template.min.css' : 'templates/styleguides/css/template.css',
        }
      }
    },
    jshint: {
      ignore_warning: {
        options: {
          '-W015': true,
        },
        src: 'javascripts/**',
        filter: 'isFile'
      }
    },
    paths: {
      src: {
        js: [
          'assets/javascripts/vendor/bootstrap/popper.js',
          'assets/javascripts/vendor/bootstrap/bootstrap.js',
          'assets/javascripts/vendor/swiper.js',
          'assets/javascripts/init.js'
        ]
      },
      dest: {
        js: 'templates/default/js/template.js',
        jsMin: 'templates/default/js/template.min.js'
      }
    },
    concat: {
      js: {
        options: {
          separator: ';',
        },
        dist: {
          src: '<%= paths.src.js %>',
          dest: '<%= paths.dest.jsMin %>',
        },
      }
    },
    uglify: {
      options: {
        compress: false,
        mangle: true
      },
      target: {
        src: '<%= paths.src.js %>',
        dest: '<%= paths.dest.js %>'
      }
    },
    webfont: {
      icons: {
        src: 'assets/icons/*.svg',
        dest: 'assets/stylesheets/fonts',
        options: {
          font: 'fonticon',
          fontFilename: 'fonticon',
          types: 'woff',
          embed: true,
          htmlDemo: false,
          stylesheet: 'scss',
          normalize: true,
          templateOptions: {
            baseClass: 'ico',
            classPrefix: 'ico-',
            mixinPrefix: 'glyph-'
          }
        }
      }
    },
    watch: {
      script: {
        files: 'assets/javascripts/*.js'
      },
      css: {
        files: [
          'assets/stylesheets/*.sass',
          'assets/stylesheets/*/*.sass',
          'assets/stylesheets/*/*.scss',
        ],
        tasks: ['sass','cssmin','jshint','concat','uglify']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');       // Validate files with JSHint
  grunt.loadNpmTasks('grunt-path');
  grunt.loadNpmTasks('grunt-contrib-concat');       // Concatenate files
  grunt.loadNpmTasks('grunt-contrib-uglify');       // Minify Javascript file
  grunt.loadNpmTasks('grunt-webfont');
  grunt.loadNpmTasks('grunt-contrib-watch');


  grunt.registerTask('default', ['watch']);
  grunt.registerTask('webfonts', ['webfont']);

};
